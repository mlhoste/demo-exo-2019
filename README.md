# Exercice : Branches/merges

# Préambule
* Vérifier la configuration de `git` (username, mail, mergetool, etc...)
    * Pour le mergetool, la commande `git config --list` doit faire apparaitre :
        * `merge.tool=meld`
        * `mergetool.meld.path=C:\Program Files (x86)\Meld\Meld.exe` (Windows)
    * Si ce n'est pas le cas vous devez le configurer :
        * Installation de Meld (évidemment !)
        * Windows (Vérifier les paths)

```
git config --global diff.tool meld
git config --global difftool.meld.path "C:\Program Files (x86)\Meld\Meld.exe"
git config --global difftool.Prompt false

git config --global merge.tool meld
git config --global mergetool.meld.path "C:\Program Files (x86)\Meld\Meld.exe"
git config --global mergetool.Prompt false
```

        * UNIX (Vérifier les paths)

```
git config --global diff.tool meld
git config --global difftool.meld.path "/usr/bin/meld"
git config --global difftool.Prompt true

git config --global merge.tool meld
git config --global mergetool.meld.path "/usr/bin/meld"
git config --global mergetool.Prompt true
```

* Cloner le dépôt 
    * `git clone https://gitlab.com/mlhoste/demo-exo-2019`

# Etape 1 :
* Se positionner sur la branche `dev2`
    * `git checkout dev2`
* Créer une branche `votre_nom` à partir de `dev2` et se placer dessus
    * `git checkout -b votre_nom`
    * ou `git branch votre_nom` puis `git checkout votre_nom`
* Modifier `file1.txt`
* Créer un fichier `file4.txt` dans `dir1/`
* Supprimer `file2.txt`
* Faites vos commits (ajout à l'index + commit)
    * `git add .`
    * `git commit -m "message..."`
* **Mise en commun**

# Etape 2:
* Revenir sur `dev2`
    * `git checkout dev2`
* Modifier `file1.txt`
* Modifier `file2.txt`
* Modifier `file3.txt`
* Faites vos commits (ajout à l'index + commit)
    * `git add .`
    * `git commit -m "message..."`
* Afficher l'historique
    * `git log`
    * Vous ne devriez voir que le commit de l'étape 2 (+ mes commits !), celui de l'étape 1 ayant été fait sur votre branche perso, pas sur dev.
* **Mise en commun**

# Etape 3:
* Merger la branche `votre_nom` sur `dev2`
    * `git merge votre_nom`
* Gérer les conflits potentiels
    * Pour lancer le mergetool : `git mergetool`
* Vérifier que tout est OK
    * Plus de `>>>>` ou `<<<<` dans les fichiers `*.txt` (ces caractères sont ajoutés par `git` pour signaler qu'il y a eu une fusion)
* Commiter le résultat du merge
    * Vous aurez peut être besoin d'ajouter un fichier `.gitignore` pour ne pas commiter les fichiers `*.orig`
* Afficher l'historique. L'historique vous parait-il "propre" ?
    * Vous devriez voir apparaitre les commits de l'étape 1 et 2 grâce à la fusion des 2 branches !
* **Mise en commun**
